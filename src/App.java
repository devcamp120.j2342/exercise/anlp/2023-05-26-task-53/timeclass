import models.Time;

public class App {
    public static void main(String[] args) throws Exception {
        Time time1 = new Time(10,20,59);
        Time time2 = new Time(8,30,00);
        System.out.println("Subtask 3");
        System.out.println(time1.toString());
        System.out.println(time2.toString());

        System.out.println("Subtask 4");
        time1.nextSecond();
        time2.previousSecond();
        System.out.println(time1.toString());
        System.out.println(time2.toString());

    }
}
